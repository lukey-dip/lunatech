package controllers.forms

object CountrySearchForm {
  import play.api.data.Forms._
  import play.api.data.Form

  /**
   * A form processing DTO that maps to the form below.
   *
   */
  case class Data(sText: String)

  /**
   * Specifies the form fields and their types,
   * as well as how to convert from a Data to form data and vice versa.
   */
  val form = Form(
    mapping(
      "sText" -> nonEmptyText
    )(Data.apply)(Data.unapply)
  )
}