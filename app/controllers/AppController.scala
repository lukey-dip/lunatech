package controllers

import javax.inject._

import javax.inject.Inject

import controllers.forms._

import play.api.data._
import play.api.i18n._

import play.api.Logger

import play.api.mvc._
import scala.collection.mutable.ListBuffer

import luke.lunatech.DataManager

/**
 * AppController using MessagesAbstractController.
 *
 * Instead of MessagesAbstractController, you can use the I18nSupport trait,
 * which provides implicits that create a Messages instances from
 * a request using implicit conversion.
 *
 * See https://www.playframework.com/documentation/2.6.x/ScalaForms#passing-messagesprovider-to-form-helpers
 * for details.
 */
class AppController @Inject() (cc: MessagesControllerComponents) extends MessagesAbstractController(cc) {

	/** Data instance */
	val dm = new DataManager()

	/** Action for dev and debug */
	def dev = Action {
    	Ok(views.html.simpleMessage("Dev page"))
  	}

  	/** Home page redirect to search page */
	def index() = Action {
    	Redirect(routes.AppController.startSearch)
  	}

  	/** Action for fuzzy search on country name or code */
	def search(search: Option[String]) = Action {
    	search match {
    		case Some(s) => Ok(views.html.search(dm.findDeserializedCountries(s))(None))
    		case None => Ok(views.html.search(dm.countries.values.toList.sortBy(_.name))(None))
    	}
  	}

  	/** Display search form */
	def startSearch() = Action { implicit request: MessagesRequest[AnyContent] =>
    	// Pass an unpopulated form to the template
    	Ok(views.html.searchCountryForm(CountrySearchForm.form))
  	}

	/** views.html.searchCountryForm POST handler */
	def onCountrySearch() = Action { implicit request: MessagesRequest[AnyContent] =>

    	//If form data is bad display the form with the errors highlighted
    	val errorFunction = { formWithErrors: Form[CountrySearchForm.Data] =>
      		BadRequest(views.html.searchCountryForm(formWithErrors))
    	}

    	//If form data is good run search
    	val successFunction = { data: CountrySearchForm.Data =>
      		// This is the good case, where the form was successfully parsed as a Data.
      		Redirect(routes.AppController.search(Option(data.sText)))
    	}

    	val formValidationResult = CountrySearchForm.form.bindFromRequest
    	formValidationResult.fold(errorFunction, successFunction)
  	}

  	/** Display country page */
  	def country(code: String) = Action {
    	dm.countries.get(code) match {
      		case Some(c) => Ok(views.html.countryWithCss(c))
      		case None => Ok("country not foud - TODO")
    	}
  	}

    /** Display country page */
    def countryWithCss(code: String) = Action {
      dm.countries.get(code) match {
          case Some(c) => Ok(views.html.countryWithCss(c))
          case None => Ok("country not foud - TODO")
      }
    }

  	/** Display runway page */
  	def runway(id: Int) = Action {

    	dm.runways.get(id) match {
      		case Some(r) => Ok(views.html.runwaysTable(List(r)))
      		case None => Ok("runway not foud - TODO")
    	}

  	}

  	/** Display report page */
  	def report = Action {
  	  	val sortedList = dm.countries.toSeq.sortBy(_._2.airports.length)
  	  	val most 	= ListBuffer[(Int, String, Int, String)]()
  	  	val least = ListBuffer[(Int, String, Int, String)]()
		
  	  	// Countries with the least airports
  	  	var rank = 0
  	  	for (v <- sortedList.take(10).toList) {
  	  	  	least += ((sortedList.length - rank, v._2.name, v._2.airports.length, v._2.code))
  	  	  	rank += 1
  	  	}
	
  	  	// Countries with the most airports
  	  	rank = 10
  	  	for (v <- sortedList.takeRight(10).toList) {
  	  	  	most += ((rank, v._2.name, v._2.airports.length, v._2.code))
  	  	  	rank -= 1
  	  	}
		
  	  	val runways = DataManager.getRunwayTypes(dm.countries.values.toList)
  	  	val bonus = views.html.runwayIdentsReport("The top 10 most common runway identifications", DataManager.getRunwayIdents(dm.runways.values.toList).sortBy(_._2).reverse, 10)
		
  	  	Ok(views.html.report(most.toList.reverse, least.toList.reverse, sortedList.length)(runways.sortBy(_._2).reverse)(List(bonus)))
  	}

  	/** Display runway information for a country */
  	def countryReport(code: String) = Action {
    	dm.countries.get(code) match {
      		case Some(country) => Ok(views.html.countryReport(country))
      		case None => Ok(s"Country code $code not found")
    	}
  	}
}