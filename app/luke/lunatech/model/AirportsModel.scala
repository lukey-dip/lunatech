package luke.lunatech.model

import scala.collection.mutable.ListBuffer

/**
 * This class represents the minimum data required for this app to work
 * with properties populated from a row in airports.csv.  
 *
 * @param id integer identification of the airport in the db
 * @param name of the airport
 * @param country location of the airport by country name
 * @param runways collection of runway
 */
case class Airport(override val id:Int, val name:String, val country:String, val runways:ListBuffer[Runway]) extends Identifiable[Int]( id ) 

/**
 * This class represents the minimum data required for this app to work
 * with properties populated from a row in runways.csv.  
 *
 * @param id integer identification of the runway in the db
 * @param le_ident identification of the runway type
 * @param surface description of the runway surface
 */
case class Runway(override val id: Int, val le_ident:Option[String], val surface:Option[Surface]) extends Identifiable[Int](id)

case class Surface(val name: String) {

	/*
	 * TODO: Case class for Surface is probably overkill for brief. If time I may refactor
	 * to remove case class and just use a Option[String]
	 *
	 * If I refactor I don't need the toString override for HTML template
	 */
	override def toString = name
}

/**
 * This class represents the minimum data required for this app to work
 * with properties populated from a row in countries.csv.  
 *
 * @param id integer identification of the country in the db
 * @param name of the country
 * @param code two letter internation country identification
 * @param airports collection of airport data in the country
 */
case class Country(override val id:Int, val name: String, val code:String, val airports:ListBuffer[Airport]) extends Identifiable[Int](id)

object AirportType extends Enumeration {
      type AirportType = Value
      val Heliport, SmallAirport, Unknown = Value

      def getTypeByName(name:String):Value = 
            values.find(_.toString.toLowerCase == name.toLowerCase()).getOrElse(Unknown)
}