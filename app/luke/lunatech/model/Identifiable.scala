package luke.lunatech.model

class Identifiable[IdType](val id: IdType){}