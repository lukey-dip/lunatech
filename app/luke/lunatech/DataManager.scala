package luke.lunatech

import java.io._
import scala.io.Source
import scala.collection.mutable.HashMap
import scala.collection.mutable.ListBuffer
import scala.util.control.Breaks._
import play.api.{ Logger, MarkerContext }

import luke.lunatech.model._

object DataManager {

	val PATH_COUNTRIES = "csv_data/countries.csv"
	val PATH_RUNWAYS = "csv_data/runways.csv"
	val PATH_AIRPORTS = "csv_data/airports.csv"

	/**
	 * Read data from CSV and parse to a list of Map key value pairs
	 * where each Map represents a row, and each k,v pair in the map is
	 * the column heading -> value
	 *
	 * @param path path to CSV file to read
	 */
	def fetchData(path:String):List[Map[String, String]] = {
		val reader = com.github.tototoshi.csv.CSVReader.open(new File(path))
		val out = reader.allWithHeaders()

		reader.close()
		out
	}

	/**
	 * Read data from CSV and parse to a list of Map key value pairs if
	 * the predicate p returns true. Each Map represents a row, and each 
	 * k,v pair in the map is the column heading -> value.
	 *
	 * @param path path to CSV file to read
	 * @param p the predicate used to test elements
	 * @return a new immutable List of Map[String, String] containing the column heading and cell value that satisfy the predicate p
	 */
	def fetchData(path:String, p: Map[String, String] => Boolean):List[Map[String, String]] = {
		val out = new ListBuffer[Map[String, String]]()
		val reader = com.github.tototoshi.csv.CSVReader.open(new File(path))
		for(field <- reader.allWithHeaders()) {
			if(p(field)) {
				out += field
			} 
		}
		reader.close()
		out.toList
	}

	/**
	 * Creates a list of runway surface types for a collection of Country
	 * instances and sums the total number of runways for each surface type
	 *
	 * @param countries List of Country intances
	 * @return a new immutable List of (String, Int) key value pairs where the key is the runway surface type and the value is the sum of all the runways of that type
	 */
	def getRunwayTypes(countries:List[Country]):List[(String, Int)] = {

		// TODO: can probably use filter
		def getRunwaySurfaces(country:Country):List[Surface] = {
			val out = ListBuffer[Surface]()
			country.airports.foreach(a => a.runways.foreach(r => r.surface match {
				case Some(s) => out += s
				case None => 
				}))
			out.toList
		}

		var out = scala.collection.mutable.Map[String, Int]()
		countries.foreach(c => getRunwaySurfaces(c).foreach(s =>
			//TODO: poss exract to function... similar in getRunwayIdents 
			out.get(s.name) match {
			case Some(pair) => out.apply(s.name) += 1
			case None => out += ((s.name, 1))
		}))
		out.map { case (k,v) => (k, v) }(collection.breakOut): List[(String, Int)]
	}

	/**
	 * Creates a collection of runway indentification types for a collection of Runway
	 * instances and sums the total number for each identification type
	 *
	 * @param countries List of Runway intances
	 * @return a new immutable List of (String, Int) key value pairs where the key is the runway ident type and the value is the sum of all the runways of that type
	 */
	def getRunwayIdents(runways:List[Runway]):List[(String, Int)] = {
		val out = scala.collection.mutable.Map[String, Int]()

		runways.foreach(runway => runway.le_ident match {
			case Some(id) => 
				//TODO: poss exract to function... similar in getRunwayTypes
				out.get(id) match {
				case Some(pair) => out.apply(id) += 1
				case None => out += ((id, 1))
			}
			case None => 
		})

		out.map { case (k,v) => (k, v) }(collection.breakOut): List[(String, Int)]
	}
}

class DataManager 
{
	protected val logger = Logger(this.getClass)

	/**  Store semi-unstructured country CSV data */
	private var countriesList: List[Map[String,String]] = Nil
	/**  Store semi-unstructured airports CSV data */
	private var airportsList: List[Map[String,String]] = Nil
	/**  Store semi-unstructured runway CSV data */
	private var runwaysList: List[Map[String,String]] = Nil

	/** Deserialized country data in HashMap for quick read/write */
	private val _countries = HashMap[String, Country]()
	/** Immutable HasMap of Airport instances. Key = country code id */
 	def countries:scala.collection.immutable.HashMap[String, Country] = scala.collection.immutable.HashMap[String, Country]() ++ _countries
 	// def countries = _countries

 	/** Deserialized airport data in HashMap for quick read/write */
 	private val _airports = HashMap[Int, Airport]()
 	/** Immutable HasMap of Airport instances. Key = airport id */
 	def airports:scala.collection.immutable.HashMap[Int, Airport] = scala.collection.immutable.HashMap[Int, Airport]() ++ _airports
 	// def airports = _airports

 	/** Deserialized airport data in HashMap for quick read/write */
 	private val _runways = HashMap[Int, Runway]()
 	/** Immutable HasMap of Airport instances. Key = runway id */
 	def runways:scala.collection.immutable.HashMap[Int, Runway] = scala.collection.immutable.HashMap[Int, Runway]() ++ _runways
 	// def runways = _runways

 	fetchData()
 	deserializeData()


 	/** Read data from CSV files */
	private def fetchData():Unit = {
		airportsList = DataManager.fetchData(DataManager.PATH_AIRPORTS)
		countriesList = DataManager.fetchData(DataManager.PATH_COUNTRIES)
		runwaysList = DataManager.fetchData(DataManager.PATH_RUNWAYS)
	}

	/** Deserialize CSV data. Requires fetchData to be called */
	private def deserializeData() = {
		logger.trace("deserializeData")
		//Parse countries
		for(country <- countriesList)
		{
			val c = createCountry(country)
			_countries.put(c.code, c)
		}

		//Parse airports		
		for(airport <- airportsList)
		{
			val a = createAirport(airport)
			_airports.put(a.id, a)

			_countries.get(a.country) match {
				case Some(country) => country.airports += a
				case None =>
			}
		}

		var airportcode:Int = 0
		for(runway <- runwaysList)
		{
			val r = createRunway(runway)
			_runways.put(r.id, r)
			
			airportcode = runway.get("airport_ref").getOrElse("").toInt
			_airports.get(airportcode) match {
				case Some(airport) => airport.runways += r
				case None =>
			}
		}

		var totalRunways = 0
		for(a <- _airports)
		{
			// println(a._2.name + " has " + a._2.runways.length + " runways")
			totalRunways += a._2.runways.length
		}

		var totalAirports = 0
		for(c <- _countries)
		{
			totalAirports += c._2.airports.length
		}
	}

	/**
	 * Desrializes Map of country data
	 *
	 * @param data the semi-structured CSV country data
	 * @return the resulting Country instance
	 */
	private def createCountry(data:Map[String,String]):Country = {
		val id = data.get("id").getOrElse("").toInt
		val name = data.get("name").getOrElse("")
		val code = data.get("code").getOrElse("")

		Country(id, name, code, ListBuffer[Airport]())
	}

	/**
	 * Desrializes Map of airport data
	 *
	 * @param data the semi-structured CSV airport data
	 * @return the resulting Airport instance
	 */
	private def createAirport(data:Map[String,String]):Airport = {
		val id = data.get("id").getOrElse("").toInt
		val name = data.get("name").getOrElse("")
		val country = data.get("iso_country").getOrElse("")
		
		Airport(id, name, country, ListBuffer[Runway]())
	}

	/**
	 * Desrializes Map of runway data
	 *
	 * @param data the semi-structured CSV runway data
	 * @return the resulting Runway instance
	 */
	private def createRunway(data:Map[String,String]):Runway = {

		val id = data.get("id").getOrElse("").toInt
		val ident = data.get("le_ident") match {
			case Some(lIdent) => Option(lIdent)
			case None => None
		}
		val surface = data.get("surface") match {
			case Some(s) => Option(Surface(s))
			case None => None
		}

		Runway(id, ident, surface)
	}

	/**
	 * Provides a collection of Runway instances
	 *
	 * @return an immutable List of Runway instances
	 */
	def getAllRunways:List[Runway] = {
		var out = ListBuffer[Runway]()
		for((k,v) <- _airports)
		{
			for(r <- v.runways)
				out += r
			// out = ListBuffer.concat(out, v.runways) //concat is slow!! I guess because it's creating a new list each time
		}
		out.toList
	}

	/**
	 * Fuzzy search of deserialized CSV data for a country by name or country code
	 * 
	 * @param str a search string
	 * @return an immutable List of Countries matching search 
	 */
	def findDeserializedCountries(str:String):List[Country] = {
		var out = Map[Int, Country]()

		def addToList(country:Country) = {
			out.get(country.id) match {
				case None => out = out + (country.id -> country)
				case _ =>
			}
		}

		_countries.get(str.toUpperCase) match {
			case Some(country) => {
				addToList(country)
			}
			case None => 
		}

		var pattern = str.toLowerCase.r
		for ((c,v) <- _countries)
		{
			
			if(pattern.findFirstIn(v.name.toLowerCase) != None)
			{
				addToList(v)
			}
		}

		out.values.toList
	}


	//////////////////////////
	//						//
	//	UNIT TESTING UTILS	//
	//						//
	//////////////////////////


	/** These accessors remain for unit tests of CSV data before deserialization */
	def allCountries():List[Map[String, String]] = countriesList
	def allAirports():List[Map[String, String]] = airportsList
	def allRunways():List[Map[String, String]] = runwaysList


	/**
	 * Fuzzy search of semi-structured CSV data for a country by name or country code
	 *
	 * TODO: re-write using filter and a predicate
	 * 
	 * @param str a search string
	 * @return an immutable List of Maps matching search 
	 */
	def findCountries(str:String):List[Map[String, String]] = {
		val out = ListBuffer[Map[String, String]]()
		var pattern = str.toLowerCase.r

		def addToList(map:Map[String, String]) = {
			if(!out.contains(map)){
				// println(s"adding country: $map")
				out += map
			} 
		}

		for (p <- countriesList) {
			
			var code = p.get("code").getOrElse("")
			if(pattern.findFirstIn(code.toLowerCase) != None)
			{
				addToList(p)
			}

			val name = p.get("name").getOrElse("")
			if(pattern.findFirstIn(name.toLowerCase) != None)
			{
				addToList(p)
			}
		}

		out.toList
	}

	/**
	 * Search of semi-structured CSV data for a country by country code
	 *
	 * TODO: write an 'any' field search with a predicate
	 * TODO: write deserialzed data version for testing
	 * 
	 * @param code a country code to search
	 * @return an immutable List of Maps matching search 
	 */
	def getAirportsByCountryCode(code:String):List[Map[String, String]] = airportsList.filter(a => a.get("iso_country").getOrElse("") == code).toList
}