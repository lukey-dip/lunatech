# the build target executable:
all: 
	sbt test
	sbt run

compile:
	sbt compile

clean:
	sbt clean