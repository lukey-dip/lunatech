# lunatech airports example

# CLONE, COMPILE AND RUN
Open a terminal, go to an empty directory and run these commmands in sequence..
git clone https://lukey-dip@bitbucket.org/lukey-dip/lunatech.git
cd lunatech
make

Once the unit tests have been completed succesfully the script will launch a localhost.
To execute the program, open a web browser and go to http://localhost:9000.

# AUTHOR
WOODCOCK Luke lukedillonwoodcock@gmail.com

# DEPENDENCIES (NOT INCLUDED IN play-scala-starter-example)
com.github.tototoshi for parsing csv files

# UNIT TESTS
This project uses ScalaTest for unit testing. To run the test, type command 'sbt test'.
