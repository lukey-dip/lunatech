// import akka.actor.ActorSystem
// import controllers.{AsyncController, CountController}
import org.scalatestplus.play._
import play.api.test.Helpers._
import scala.util.control.Breaks._
import scala.collection.mutable.ListBuffer
import luke.lunatech.DataManager
// import play.api.test.FakeRequest
// import services.Counter

/**
 * Unit tests can run without a full Play application.
 */
class CountrySpec extends PlaySpec {

  val dm = new DataManager()
  val totalNumberOfCountries = dm.allCountries.length

  "Countries" should {

    "have a count of 247" in {
       dm.allCountries.length must be(totalNumberOfCountries)
    }

    "have no missing properties for deserialization" in {
      val countries = DataManager.fetchData(DataManager.PATH_COUNTRIES, a => a.get("name").getOrElse("") != "" && a.get("code").getOrElse("") != "")
      countries.length must be(totalNumberOfCountries)
    }

    "not have duplicates" in {

      var isCorrect = true
      for(c <- dm.allCountries)
      {
        val code = c.get("code").getOrElse("")
        if(dm.allCountries.filter(p => p.get("code").getOrElse("") == code).length > 1)
        {
          isCorrect = false
          break
        }
      }

      isCorrect must be(true)
    }
  }

  "Fuzzy searching" should {
    var l = dm.findCountries("US") 
    "find 7 results with search string 'US'" in {
      l.length must be(7)
    }

    "find 1 results with search string 'greenland'" in {
      l = dm.findCountries("greenland")
      l.length must be(1)
    }

    "and the country code" should {
      "be 'GL'" in {
        l(0).get("code").getOrElse("") must be("GL")
      }
    }
  }

  "Searching 'zimb'" should {
    var l = dm.findCountries("zimb")
    "find 'zimbabwe'" in {
      l(0).get("name").getOrElse("") must be("Zimbabwe")
    }
  }

  var desl = dm.findDeserializedCountries("US") 
  "find 7 results in deserialized data with search string 'US'" in {
    desl.length must be(7)
  }
}
