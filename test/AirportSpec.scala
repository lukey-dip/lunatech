import org.scalatestplus.play._
import play.api.test.Helpers._
import scala.collection.mutable.ListBuffer
import luke.lunatech.DataManager

/**
 * Unit tests for airport data.
 */
class AirportSpec extends PlaySpec {

  val dm = new DataManager()
  val totalNumberOfAirports = dm.allAirports.length

  //WARNING: If data is expected to change this test will be meaningless 
  val expectedNumberOfUSAirports = 21501

  "Airports" should {

    "have the property 'id'" in {
      val airports = DataManager.fetchData(DataManager.PATH_AIRPORTS, a => a.get("id").getOrElse("") != "")
      airports.length must be(totalNumberOfAirports)
    }

    "have the property 'iso_country'" in {
      val airports = DataManager.fetchData(DataManager.PATH_AIRPORTS, a => a.get("iso_country").getOrElse("") != "")
      airports.length must be(totalNumberOfAirports)
    }

    "have the property 'name'" in {
      val airports = DataManager.fetchData(DataManager.PATH_AIRPORTS, a => a.get("name").getOrElse("") != "")
      airports.length must be(totalNumberOfAirports)
    }

    //WARNING: If data is expected to change this test will be meaningless
    s"have a total of $expectedNumberOfUSAirports in the US" in {
      dm.getAirportsByCountryCode("US").length must be(expectedNumberOfUSAirports)
    }
  }
}
