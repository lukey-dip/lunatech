import org.scalatestplus.play._
import play.api.test.Helpers._
import scala.collection.mutable.ListBuffer
import scala.util.control.Breaks._
import luke.lunatech.DataManager

/**
 * Unit tests can run without a full Play application.
 */
class RunwaySpec extends PlaySpec {

  val dm = new DataManager()
  val data = dm.countries

  //WARNING: If data is expected to change this test will be meaningless 
  val totalNumberOfRunways = dm.allRunways.length

  "Runways" should {

    "have the property 'airport_ref'" in {
      val runways = DataManager.fetchData(DataManager.PATH_RUNWAYS, a => a.get("airport_ref").getOrElse("") != "")
      runways.length must be(totalNumberOfRunways)
    }

    // // Will fail - not all runways have surface information
    // "have the property 'surface'" in {
    //   val runways = DataManager.fetchData(DataManager.PATH_RUNWAYS, a => a.get("surface").getOrElse("") != "")
    //   runways.length must be(totalNumberOfRunways)
    // }

    // // Will fail - not all runways have le_idetn information
    // "have the property 'le_ident'" in {
    //   val runways = DataManager.fetchData(DataManager.PATH_RUNWAYS, a => a.get("le_ident").getOrElse("") != "")
    //   runways.length must be(totalNumberOfRunways)
    // }
  }

  "Every runway" should {
    
    "have an airport" in {

      //TODO: put runways in a hash table in DataManager
      val runways = DataManager.fetchData(DataManager.PATH_RUNWAYS)
      val airports = dm.airports

      var isCorrect = true
      var noAirports = 0
      breakable {
        var airport_ref:Int = 0
        for(r <- runways)
        {
          airport_ref = r.get("airport_ref").getOrElse("-1").toInt
          airports.get(airport_ref) match {
            case None => {
              val runwayId = r.get("id").getOrElse(-1)
              // println(s"Aiport $airport_ref not found for Runway $runwayId")
              noAirports += 1
              isCorrect = false
            }
            case _ =>
          }
        }
      }
      isCorrect must be(true)
      // println("Total number of runways with no airport: " + noAirports)
    }
  }

  /**
   * This test fails because not every airport has a runway
   * There are > 13000 airports without runways
   */
  // "Every airport" should {
    
  //   "have a runway" in {
  //     val runways = DataManager.fetchData(DataManager.PATH_RUNWAYS)
  //     val airports = DataManager.fetchData(DataManager.PATH_AIRPORTS)

  //     var isCorrect = true
  //     var noRunways = 0
  //     breakable {
  //       for(c <- data)
  //       {
  //         for(a <- c._2.airports)
  //         {
  //           if(a.runways.length < 1)
  //           {
  //             noRunways += 1
  //             isCorrect = false
  //             // break
  //           }
  //         }
  //       }
  //     }
  //     isCorrect must be(true)
  //   }
  // }
}
